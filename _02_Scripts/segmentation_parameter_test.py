"""
bachelor - neural networks - semantic segmentation
Segmentation Parameter Test

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""

from _02_Scripts import segmentation as seg
from _02_Scripts import yolov2 as yolo
import numpy as np
import os
import cv2

# this script can help you to find the right parameters
# of the ImageConfig in your project.
# To load your test image you have to provide the right relative path.
# Running the script the track bar window will open.
# Press ENTER to take the settings and visualize your Image.
# The visualization window has to be closed manually before you
# can set new modifications.
# Press ESC to close and print out your last parameter values

# provide path to representative image
# the lighting and environment in that image should be as
# close to reality (actual use of project) as possible
PATH = os.getcwd()
print(PATH)
img_PATH = os.path.join(PATH, '../_01_Dataset/test/test/00000004.png')


def nothing(x):
    """
    Does nothing. Callback for cv2 track bar
    :param x: current track bar value, unused
    :return: nothing
    """
    pass


# create default YoloConfig and YoloNet
yoloconfig = yolo.YoloConfig(gpu=0)
neural_net = yolo.YoloNet(yoloconfig)

# create and configure ImageConfig
imageConfig = seg.ImageConfig()

# load image from provided path
img = cv2.imread(img_PATH)
# create seg.Image object for segmentation and results
image = seg.Image(img, imageConfig)

# create window with track bars to modify parameters
# note! Not all parameters can be changed by trackbar!
# they can still be modified within this module
cv2.namedWindow('props', cv2.WINDOW_AUTOSIZE)
cv2.resizeWindow('props', 300, 800)
cv2.moveWindow('props', 0, 0)
cv2.createTrackbar('bboxOffI', 'props', imageConfig.bboxOffsetInstance, 50, nothing)
cv2.createTrackbar('thres1C', 'props', imageConfig.threshold1Canny, 255, nothing)
cv2.createTrackbar('thres2C', 'props', imageConfig.threshold2Canny, 255, nothing)
cv2.createTrackbar('minLengthH', 'props', imageConfig.minLineLengthHough, 150, nothing)
cv2.createTrackbar('maxGapH', 'props', imageConfig.maxLineGapHough, 50, nothing)
cv2.createTrackbar('thresH', 'props', imageConfig.thresholdHough, 255, nothing)
cv2.createTrackbar('shortRect', 'props', imageConfig.shortSideRectangle, 255, nothing)
cv2.createTrackbar('longRect', 'props', imageConfig.longSideRectangle, 255, nothing)
cv2.createTrackbar('shortTol', 'props', imageConfig.shortSideToleranceRectangle, 100, nothing)
cv2.createTrackbar('longTol', 'props', imageConfig.longSideToleranceRectangle, 100, nothing)


def refresh_image(image_obj):
    """
    Takes Image object, updates its config values with current track bar positions
    recreates the instances and visualizes results
    :param image_obj: seg.Image object to work with
    :return: nothing
    """
    # update ImageConfig in Image obj. with current track bar values
    # some attributes do not have a track bar, can be changed in code below here
    image_obj.config.bboxOffsetInstance = cv2.getTrackbarPos('bboxOffI', 'props')       # def 7

    image_obj.config.apertureSizeCanny = 3      # def 3
    image_obj.config.threshold1Canny = cv2.getTrackbarPos('thres1C', 'props')           # def 120
    image_obj.config.threshold2Canny = cv2.getTrackbarPos('thres2C', 'props')           # def 200

    image_obj.config.minLineLengthHough = cv2.getTrackbarPos('minLengthH', 'props')     # def 10
    image_obj.config.maxLineGapHough = cv2.getTrackbarPos('maxGapH', 'props')           # def 15
    image_obj.config.rhoHough = 1                # def 1
    image_obj.config.thetaHough = np.pi/180      # def np.pi/180
    image_obj.config.thresholdHough = cv2.getTrackbarPos('thresH', 'props')             # def 30

    image_obj.config.shortSideRectangle = cv2.getTrackbarPos('shortRect', 'props')      # def 60
    image_obj.config.longSideRectangle = cv2.getTrackbarPos('longRect', 'props')        # def 120
    image_obj.config.shortSideToleranceRectangle = cv2.getTrackbarPos('shortTol', 'props')  # def 15
    image_obj.config.shortSideToleranceFactorRectangle = 1.5     # def 1.5
    image_obj.config.longSideToleranceRectangle = cv2.getTrackbarPos('longTol', 'props')    # def 25
    image_obj.config.longSideToleranceFactorRectangle = 2.4      # def 2.4

    image_obj.config.angleToleranceResult = 5    # def 5

    # recreate instances in Image obj. so modifications take effect
    image_obj.recreate_all_instances()
    # visualize instances
    image_obj.visualize_instances()


# run inference of image in seg.Image object
# also adds found instances and automatically triggers segmentation
neural_net.predict(image)
# while loop controls keyboard input and calls refresh function
while True:
    key = cv2.waitKey(10)
    if key == 27:       # ESC to leave and print values
        break
    if key == 13:       # ENTER to update and visualize
        refresh_image(image)


cv2.destroyAllWindows()
# print all current values in image.config
print('\n\n###############################################\n\n'
      'YOUR LAST SETTINGS: \n\n'
      f'bboxOffsetInstance:                 {image.config.bboxOffsetInstance} \n\n'
      f'apertureSizeCanny:                  {image.config.apertureSizeCanny}\n'
      f'threshold1Canny:                    {image.config.threshold1Canny} \n'
      f'threshold2Canny:                    {image.config.threshold2Canny} \n\n'
      f'minLineLengthHough:                 {image.config.minLineLengthHough}\n'
      f'maxLineGapHough:                    {image.config.maxLineGapHough}\n'
      f'rhoHough:                           {image.config.rhoHough}\n'
      f'thetaHough:                         {image.config.thetaHough}\n'
      f'thresholdHough:                     {image.config.thresholdHough}\n\n'
      f'shortSideRectangle:                 {image.config.shortSideRectangle}\n'
      f'longSideRectangle:                  {image.config.longSideRectangle}\n'
      f'shortSideToleranceRectangle:        {image.config.shortSideToleranceRectangle}\n'
      f'shortSideToleranceFactorRectangle:  {image.config.shortSideToleranceFactorRectangle}\n'
      f'longSideToleranceRectangle:         {image.config.longSideToleranceRectangle}\n'
      f'longSideToleranceFactorRectangle:   {image.config.longSideToleranceFactorRectangle}\n\n'
      f'angleToleranceResult:               {image.config.angleToleranceResult}')
