"""
bachelor - neural networks - semantic segmentation
Mask R-CNN implementation (Matterport, Inc.)

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""

import os
import argparse
from os import walk
import cv2

root_dir = os.getcwd()

# initialize the parser
parser = argparse.ArgumentParser(description='Resize images (.jpeg, .jpg or .png) in given input directory')
# add the command line parameters positional/optional
parser.add_argument('-width', help='width of output images')
parser.add_argument('-height', help='height of output images')
parser.add_argument('-input_dir', '-id', help='relative path of input images to current path')
parser.add_argument('-output_dir', '-od', help='relative path of output images to current path')
parser.add_argument('-start_num', help='default start number for output filename is 00000000')

args = parser.parse_args()
print(args)

if args.width:
    width = int(args.width)
else:
    width = 640
    print('\n\ntaking default width of 640 pixels\n')
if args.height:
    height = int(args.height)
else:
    height = 640
    print('taking default height of 640 pixels\n')
if args.input_dir:
    input_dir = os.path.join(root_dir, args.input_dir)
else:
    input_dir = root_dir
    print('taking script directory as input_dir\n')
if args.output_dir:
    output_dir = os.path.join(root_dir, args.output_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
        print('output_dir created!')
else:
    output_dir = root_dir
    print('taking script directory as output_dir\n')
if args.start_num:
    filenum = int(args.start_num)
    print('starting at number ', filenum)
else:
    filenum = 0

print("running...")

images = []
ext = (".jpeg", ".jpg", ".png")

for (dirpath, dirnames, filenames) in walk(input_dir):
    for filename in filenames:
        if filename.endswith(ext):
            images.append(os.path.join(dirpath, filename))

for image in images:
    img = cv2.imread(image, cv2.IMREAD_UNCHANGED)

    h, w = img.shape[:2]
    # pad_bottom, pad_right = 0, 0
    ratio = w / h

    if h > height or w > width:
        interp = cv2.INTER_AREA
    else:
        interp = cv2.INTER_CUBIC

    w = width
    h = round(w / ratio)

    if h > height:
        h = height
        w = round(h * ratio)
        # pad_bottom = abs(height - h)
        # pad_right = abs(width - w)

    scaled_img = cv2.resize(img, (w, h), interpolation=interp)
    # padded_img = cv2.copyMakeBorder(scaled_img, 0, pad_bottom, 0, pad_right, borderType=cv2.BORDER_CONSTANT, value=[0, 0, 0])
    filenum_str = str(filenum)
    if len(filenum_str) < 8:
        diff = 8 - len(filenum_str)
        for zeros in range(0, diff):
            filenum_str = "0" + filenum_str

    imgname = filenum_str + '.png'
    cv2.imwrite(os.path.join(output_dir, imgname), scaled_img)

    filenum += 1
print("completed!")
