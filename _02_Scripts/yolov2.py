"""
bachelor - neural networks - semantic segmentation
YOLOv2 implementation (Darkflow)

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""


import os
import sys
from _02_Scripts import segmentation as seg
from _02_Scripts import segmentation_utils as ut
# define some Paths used to change work directory
SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))
DARKFLOW_ROOT = os.path.join(SCRIPT_PATH, "../_04_yolov2/darkflow-master/")
sys.path.append(DARKFLOW_ROOT)
from darkflow.net.build import TFNet


class YoloNet:
    """
    Implementation of YOLOv2 Net with darkflow (Trieu H. Trinh),
    used for this projects purpose of detecting objects, pass the
    results into seg.Image object and trigger semantic segmentation
    process.
    Has to be configured with a YoloConfig
    """
    def __init__(self, config):
        assert isinstance(config, YoloConfig), 'argument config must be of type YoloConfig!'
        WORK_PATH = os.getcwd()     # current work directory to change back later
        os.chdir(DARKFLOW_ROOT)     # change to darkflow root directory to avoid errors related to relative paths
        self.model = TFNet(config.options)  # build actual YOLOv2 model
        os.chdir(WORK_PATH)         # change back to previous work directory

    def predict(self, image):
        """
        Executes prediction on input image.
        Input:
            image: object of type seg.Image
        Does not return anything since the results of the
        prediction are passed to the image obj as new instances.
        """

        assert isinstance(image, seg.Image), 'argument image must be of type seg.Image!'
        results = self.model.return_predict(image.image)    # get prediction results of actual image in image obj
        bboxes = ut.yolo_results_to_image_bboxes(results)   # convert yolo results to list of bboxes (seg.BoundingBox)
        print(bboxes)
        image.add_instances(bboxes)     # passes bboxes to Image obj. and triggers segmentation process

    def train(self):
        """
        Train YOLOv2 model on provided training data. All specifications
        (train=True, annotation, dataset, epoch, save, etc) have to set in
        the configuration of the model
        """
        WORK_PATH = os.getcwd()
        os.chdir(DARKFLOW_ROOT)
        self.model.train()
        os.chdir(WORK_PATH)


class YoloConfig:
    """
    Configuration class for YoloNet.
    Accepted arguments: imgdir, binary, config, dataset,
        labels, backup, summary, annotation, threshold,
        model, trainer, momentum, verbalise, train, load,
        savepb, gpu, gpuName, lr, keep, batch, epoch, save,
        demo, queue, json, saveVideo, pbLoad, metaLoad
    More detailed information to find in Project WIKI:
    https://gitlab.com/zientek.creation/bachelor-neural-networks-semantic-segmentation/-/wikis/Training/02-YOLOv2
    """
    def __init__(self, model="cfg/tiny-yolo-voc-workobj.cfg", labels="labels.txt", threshold=0.85, load=-1,
                 gpu=1.0, **kwargs):
        self.root_path = DARKFLOW_ROOT
        # set some mandatory or recommended options
        self.options = {"model": os.path.join(self.root_path, model),
                        "labels": os.path.join(self.root_path, labels),
                        "threshold": threshold,
                        "load": load,
                        "gpu": gpu}
        # extend options buy additional passed arguments
        self.options.update(kwargs)
        print(self.options)
