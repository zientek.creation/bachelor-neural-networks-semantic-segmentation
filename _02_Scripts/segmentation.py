"""
bachelor - neural networks - semantic segmentation
Mask R-CNN implementation (Matterport, Inc.)

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""

import numpy as np
import cv2
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from copy import deepcopy

from _02_Scripts import segmentation_utils as ut


class Image:
    """
    One of the main classes of this project.
    Takes image (numpy/opencv) and ImageConfig as input.
    Wraps all information about an Image in one object.
    If it's passed to a neural network to detect objects, the model.prediction() method
    passes its results into image.add_instances() which itself triggers the segmentation
    process for each added instance. The Image object creates Instance objects which
    themselves try to find the accurate position and orientation of the object in the
    detected area and create the results which contain a Rectangle object and gripping
    points of the object.
    """
    def __init__(self, image, config):
        assert isinstance(config, ImageConfig), 'config must be of type ImageConfig!'
        self.config = ImageConfig()
        # resize image to configuration values
        self.image = ut.resize_image(image, self.config)
        # copy original image as result image. If instances get
        # added, results are gonna be drawn on the result image
        self.resImage = self.image.copy()
        self.instances = []

    def visualize_result(self):
        """
        Visualizes Image.resImage. If there is no instances added, it's the
        same image as the original image the Image object was created for.
        :return: nothing
        """
        # get number of instances
        count = len(self.instances)
        # create matplotlib.pyplot
        plt.figure(figsize=(self.config.figureSizeVisualize, self.config.figureSizeVisualize))
        plt.imshow(cv2.cvtColor(self.resImage, cv2.COLOR_BGR2RGB))
        plt.axis('off')     # turn off 'axis' to show image
        # put text '*** no results ***' in every box where there was found a result by the
        # neural net but not in segmentation (no gripping points could be calculated)
        for instance in self.instances:
            if instance.instanceResult.rectangle is None:
                offset = instance.shiftedBbox.points()
                plt.text(offset[1] + .5 * (offset[3] - offset[1]), offset[0] + .5 * (offset[2] - offset[0]),
                         '*** no results ***', ha='center', va='center', size=self.config.figureSizeVisualize, alpha=.7)
        # put text '*** no instances to show ***' in the center of the
        # image if Image object does not contain any results
        if not count:
            plt.text(.5 * np.shape(self.image)[1], .5 * np.shape(self.image)[0], '*** no instances to show ***',
                         ha='center', va='center', size=self.config.figureSizeVisualize * 2, alpha=.7)
        plt.show()

    def get_results(self):
        """
        :return: list of all instanceResults in Image object
        """
        return [instance.instanceResult for instance in self.instances]

    def get_gripping_points(self):
        """
        :return: list of all found gripping Point pairs
        """
        return [instance.instanceResult.grippingPoints for instance in self.instances if
                instance.instanceResult.grippingPoints is not None]

    def add_instances(self, bbox_list):
        """
        Creates Instance for each passed BoundingBox object in bbox_list.
        Updates resImage with added results.
        :param bbox_list: list of BoundingBox objects
        :return: nothing, adds items to attribute list
        """
        for bbox in bbox_list:
            assert isinstance(bbox, BoundingBox), 'all elements in argument bbox_list must be of type BoundingBox!'
            self.instances.append(Instance(self, bbox))
        self.recreate_res_image()

    def get_instances(self):
        """
        :return: list of Instance objects
        """
        return self.instances

    def recreate_all_instances(self):
        """
        recreates all instances (might be useful, if something if
        there's some applied changes in the config)
        :return: nothing
        """
        if len(self.instances):
            for instance in self.instances:
                instance.recreate_instance(self)
        self.recreate_res_image()

    def recreate_res_image(self):
        """
        Takes a new copy of the original image and draws
        all BoundingBoxes and instanceResults on it.
        :return: nothing
        """
        self.resImage = deepcopy(self.image)
        # draw bbox and instance results on new resImage for each instance
        for instance in self.instances:
            assert isinstance(instance, Instance)
            x1, y1, x2, y2 = instance.shiftedBbox.points()
            cv2.rectangle(self.resImage, (y1, x1), (y2, x2),
                          self.config.rectColorResImage, self.config.lineThicknessImageHough)
            offset = instance.shiftedBbox.point(0)
            ut.draw_results_res_image(self.resImage, instance.instanceResult, self.config, offset=offset)

    def visualize_instances(self):
        """
        Visualizes all instances of Image object. Shows big resImage at the top
        and below instance images of all steps of the segmentation process.
        :return: nothing
        """
        count = len(self.instances)     # get number of instances
        axes = []                       # create list for plt.subplot()s
        grid_offset = 5     # offset of grid rows (number of columns defined by steps in segmentation process)
        # create figure with certain size
        plt.figure(figsize=(self.config.figureSizeVisualize, self.config.figureSizeVisualize + count))
        grid = gridspec.GridSpec(grid_offset + count, grid_offset)  # grid allows easy configuration of order
        # first subplot takes top rows of the grid, showing the resImage
        axes.append(plt.subplot(grid[0:grid_offset, :]))
        axes[0].imshow(cv2.cvtColor(self.resImage, cv2.COLOR_BGR2RGB))
        axes[0].axis('off')
        # put text '*** no results ***' in every box where there was found a result by the
        # neural net but not in segmentation (no gripping points could be calculated)
        for instance in self.instances:
            if instance.instanceResult.rectangle is None:
                offset = instance.shiftedBbox.points()
                plt.text(offset[1] + .5 * (offset[3] - offset[1]), offset[0] + .5 * (offset[2] - offset[0]),
                         '*** no results ***', ha='center', va='center', size=self.config.figureSizeVisualize, alpha=.7)
        # put text '*** no instances to show ***' in the center of the
        # image if Image object does not contain any results and return
        if not count:
            axes[0].text(.5 * np.shape(self.image)[1], .5 * np.shape(self.image)[0], '*** no instances to show ***',
                         ha='center', va='center', size=self.config.figureSizeVisualize*2, alpha=.7)
            plt.show()
            return
        # fill the grid visualizing all the single instances
        for idx in range(count):
            axes.append(self.instances[idx].visualize(grid, idx, self.config, grid_offset=grid_offset, show=False))
        plt.tight_layout()
        plt.show()

    def __repr__(self):
        return "class for semantic segmentation of objects in images"


class ImageConfig:
    """
    Class to configure all parameters regarding segmentation process, visualization, etc.
    This configuration class is specifically needed to create an Image object.
    """
    def __init__(self):
        # image is resized to one of these values, keeping the ratio
        self.widthImage = 640
        self.heightImage = 640
        # extend the bboxes provided by the neural network
        self.bboxOffsetInstance = 7
        # color of rectangles on resImage
        self.rectColorResImage = (0, 140, 255)
        # parameters for Canny edge detection in segmentation process
        # is very important to get adapted to the actual
        # conditions (lighting, contrast of work object to background, etc.)
        self.apertureSizeCanny = 3
        self.threshold1Canny = 120
        self.threshold2Canny = 200
        # parameters for Hough transform in segmentation process
        # important to adapt them to the actual conditions like background and size of work object
        self.minLineLengthHough = 10
        self.maxLineGapHough = 15
        self.rhoHough = 1
        self.thetaHough = np.pi/180
        self.thresholdHough = 30
        # visualization of segmentation steps in instances
        self.lineColorImageHough = (140, 80, 255)
        self.lineThicknessImageHough = 2
        # visualization of results in instances
        self.lineColorImageResult = (255, 30, 140)
        self.lineThicknessImageResult = 3
        self.centerPointColorImageResult = (255, 200, 140)
        self.centerPointRadiusImageResult = 4
        self.grippingPointColorImageResult = (255, 255, 255)
        self.grippingPointRadiusImageResult = 5
        # parameters for rectangle, size and tolerances
        self.shortSideRectangle = 60
        self.longSideRectangle = 120
        self.shortSideToleranceRectangle = 15
        self.shortSideToleranceFactorRectangle = 1.5
        self.longSideToleranceRectangle = 25
        self.longSideToleranceFactorRectangle = 2.4
        # tolerance of right angles
        self.angleToleranceResult = 5
        # size of visualization
        self.figureSizeVisualize = 12

    def __repr__(self):
        return "config for Image class"


class Instance:
    """
    Every Bbox passed to an Image object creates an Instance object.
    The segmentation takes place in this class.
    Calculates the results and contains all information for visualization.
    """
    def __init__(self, parent, bbox):
        assert isinstance(bbox, BoundingBox), 'argument bbox must be of type BoundingBox!'
        assert isinstance(parent, Image), 'argument parent must be of type Image!'
        self.bbox = bbox
        # extend bbox by defined offset value, since bboxes predicted
        # by neural net often cut out part of the detected object
        self.shiftedBbox, self.bboxOffset = ut.create_shifted_bbox(self.bbox, parent.config.bboxOffsetInstance,
                                                                   parent.image.shape)
        # segmentation process: create the images of each step of segmenation process
        self.imageSection = ImageSection(parent, self.shiftedBbox)
        self.imageGray = ImageGray(self.imageSection.image)
        self.imageCanny = ImageCanny(self.imageGray.image, parent.config)
        self.imageHough = ImageHough(self.imageSection.image, self.imageCanny.image, parent.config)
        # calculate results and create result image for instance
        self.instanceResult = InstanceResult(self.imageHough.lines, parent.config)
        self.imageResult = ImageResult(self.imageSection.image, self.instanceResult, parent.config)

    def recreate_instance(self, parent):
        """
        Recall self.__init__(...) to re-trigger segmentation process
        :param parent: parent Image object
        :return: nothing
        """
        self.__init__(parent, self.bbox)

    def get_instance_results(self):
        """
        :return: instanceResult object
        """
        return self.instanceResult

    def visualize(self, grid, idx, config, grid_offset=0, show=True):
        """
        Creates and returns list of matplotlib.pyplot.subplot objects containing
        the images of segmentation process of this instance.
        :param grid: matplotlib.gridspec object
        :param idx: specifies the row the subplots take place in the passed grid
        :param config: ImageConfig object
        :param grid_offset: offset of rows (from top)
        :param show: if True, show this list of plots and return nothing
        :return: list of plt.subplot objects
        """
        # create subplot for all the images of segmentation process in instance
        axes = [(plt.subplot(grid[grid_offset + idx, 0]))]
        plt.imshow(cv2.cvtColor(self.imageSection.image, cv2.COLOR_BGR2RGB))
        plt.axis('off')
        axes.append(plt.subplot(grid[grid_offset + idx, 1]))
        plt.imshow(self.imageGray.image, cmap='gray')
        plt.axis('off')
        axes.append(plt.subplot(grid[grid_offset + idx, 2]))
        plt.imshow(self.imageCanny.image, cmap='gray')
        plt.axis('off')
        axes.append(plt.subplot(grid[grid_offset + idx, 3]))
        plt.imshow(cv2.cvtColor(self.imageHough.image, cv2.COLOR_BGR2RGB))
        plt.axis('off')
        axes.append(plt.subplot(grid[grid_offset + idx, 4]))
        plt.imshow(cv2.cvtColor(self.imageResult.image, cv2.COLOR_BGR2RGB))
        plt.axis('off')
        # put text '*** no results ***' on result image if no results found
        if self.instanceResult.rectangle is None:
            plt.text(.5 * np.shape(self.imageResult.image)[1], .5 * np.shape(self.imageResult.image)[0],
                     '*** no results ***', ha='center', va='center', size=config.figureSizeVisualize, alpha=.7)
        # show this list of sub plots and return nothing
        if show:
            plt.tight_layout()
            plt.show()
            return
        plt.tight_layout()  # configures the subplots tight together
        return axes

    def __repr__(self):
        return "result: {}".format(self.instanceResult)


class InstanceResult:
    """
    Class to wrap all results of one instance (rectangle, gripping points)
    Takes lines provided by Hough transform in instance.imageHough, clusters
    them and tries to find a result for segmentation.
    """
    def __init__(self, lines, config):
        assert isinstance(config, ImageConfig), 'argument config must be of type ImageConfig!'
        self.lineClusters = ut.make_line_cluster(lines, config)             # cluster lines with similar angles
        self.rectangle = ut.find_res_rectangle(self.lineClusters, config)   # calculate resulting rectangle
        # extract gripping points from rectangle object if found
        if self.rectangle is not None:
            self.grippingPoints = self.rectangle.get_gripping_points()
        else:
            self.grippingPoints = None      # set to None otherwise

    def __repr__(self):
        return "grippingPoints: {}; rectangle: {}".format(self.grippingPoints, self.rectangle)


class ImageCrop:
    """
    Parent class for images of steps of segmentation in Instance.
    Provides image as attribute and simple methods.
    """
    def __init__(self, source_image):
        self.image = source_image

    def get_image(self):
        return self.image.copy()

    def shape(self):
        return np.shape(self.image)

    def __repr__(self):
        return "shape: {}".format(self.image.shape)


class ImageSection(ImageCrop):
    """
    Inherits from ImageCrop.
    Contains image crop detected as instance in original image in Image object.
    Takes parent Image object to refer to its original image and a Bbox to crop image.
    """
    def __init__(self, parent, bbox):
        assert isinstance(bbox, BoundingBox), 'argument bbox must be of type BoundingBox!'
        assert isinstance(parent, Image), 'argument parent must be of type Image!'
        x1, y1, x2, y2 = bbox.points()
        super().__init__(parent.image[x1:x2, y1:y2, :].copy())


class ImageGray(ImageCrop):
    """
    Inherits from ImageCrop.
    Creates and keeps gray scale image of input image (thought to be image of ImageSection)
    """
    def __init__(self, source_image):
        # initialize parent
        super().__init__(cv2.cvtColor(source_image, cv2.COLOR_BGR2GRAY))


class ImageCanny(ImageCrop):
    """
    Inherits from ImageCrop.
    Runs Canny Edge filter on gray scale input image (thought to be image of ImageGray)
    """
    def __init__(self, source_image, config):
        assert isinstance(config, ImageConfig), 'argument config must be of type ImageConfig!'
        # pass canny image to parent class
        super().__init__(cv2.Canny(source_image, threshold1=config.threshold1Canny, threshold2=config.threshold2Canny,
                                   apertureSize=config.apertureSizeCanny))


class ImageHough(ImageCrop):
    """
    Inhherits from ImageCrop.
    Takes Bitmap image (After Canny edge detection) and runs Hough Transform on it.
    Takes as well the crop of the original image and draws the detected lines on it.
    """
    def __init__(self, target_image, source_image, config):
        pass
        assert isinstance(config, ImageConfig), 'argument config must be of type ImageConfig!'
        # initialize parent class with crop of original image
        super().__init__(target_image.copy())
        # run Hough Transform on canny image
        self.lines = ut.hough_lines_angle_length(source_image, config)
        self.draw_lines(config)     # draw lines

    def draw_lines(self, config):
        """
        Draws lines detected by Hough Transform on containing image.
        :param config: ImageConfig object
        :return: nothing, changes content of self object
        """
        assert isinstance(config, ImageConfig), 'argument config must be of type ImageConfig!'
        if self.lines is None:
            return
        # draw all the lines on original image crop
        for line in self.lines:
            cv2.line(self.image, tuple(line.point1.point()), tuple(line.point2.point()),
                     config.lineColorImageHough, config.lineThicknessImageHough)


class ImageResult(ImageCrop):
    """
    Inherits from ImageCrop.
    Takes InstanceResult and draws it onto crop of original image.
    """
    def __init__(self, source_image, result, config):
        super().__init__(source_image.copy())   # initialize parent with passed image crop
        assert isinstance(result, InstanceResult), 'result must be of type InstanceResult!'
        assert isinstance(config, ImageConfig), 'argument config must be of type ImageConfig!'
        if result is not None:
            self.draw_rectangle(result, config)
            self.draw_gripping_points(result, config)
            self.draw_center_point(result, config)

    def draw_rectangle(self, result, config):
        """
        Draw rectangle in passed result onto original image crop.
        :param result: InstanceResult object
        :param config: ImageConfig object
        :return: nothing
        """
        if result.rectangle is not None:
            lines = result.rectangle.get_res_lines()
            for line in lines:
                cv2.line(self.image, tuple(line.point1.point()), tuple(line.point2.point()),
                         config.lineColorImageResult, config.lineThicknessImageResult)

    def draw_gripping_points(self, result, config):
        """
        Draw gripping points in passed result onto original image crop.
        :param result: InstanceResult object
        :param config: ImageConfig object
        :return: nothing
        """
        points = result.grippingPoints
        if points is not None:
            for point in points:
                # draw point as filled circle
                cv2.circle(self.image, tuple(point.point()), config.grippingPointRadiusImageResult,
                           config.grippingPointColorImageResult, thickness=-1)

    def draw_center_point(self, result, config):
        """
        Draw center point of rectangle in passed result onto original image crop.
        :param result: InstanceResult object
        :param config: ImageConfig object
        :return: nothing
        """
        if result.rectangle is not None:
            point = result.rectangle.centerPoint
            # draw point as filled circle
            cv2.circle(self.image, tuple(point.point()), config.centerPointRadiusImageResult,
                       config.centerPointColorImageResult, thickness=-1)


class Point:
    """
    A Point object is defined by x and y coordinates.
    Provides a variety of overloaded built-in operators (>, ==, ...) as
    well as some utility methods.
    """
    def __init__(self, x_init, y_init):
        assert isinstance(x_init, int), 'arguments x_init and y_init must be of type int!'
        assert isinstance(y_init, int), 'arguments x_init and y_init must be of type int!'
        self.x = x_init
        self.y = y_init

    def shift(self, x, y):
        """
        Shifts own coordinates by passed x and y values.
        :param x: x shift
        :param y: y shift
        :return: nothing
        """
        self.x += x
        self.y += y

    def point(self):
        """
        :return: list of coordinates [x, y]
        """
        return [self.x, self.y]

    def x_dist(self, other):
        """
        :param other: Point object
        :return: distance on x-axis (positive if x of passed Point is larger than x of self)
        """
        return other.x - self.x

    def y_dist(self, other):
        """
        :param other: Point object
        :return: distance on y-axis (positive if y of passed Point is larger than y of self)
        """
        return other.y - self.y

    def __repr__(self):
        return "".join(["Point(", str(self.x), ",", str(self.y), ")"])

    # overloading built-in operators (==, <, >, <=, >=)
    def __eq__(self, other):
        assert isinstance(other, Point), 'object of type Point cannot be compared to none-Point object!'
        if self.x == other.x and self.y == other.y:
            return True
        else:
            return False

    def __lt__(self, other):
        assert isinstance(other, Point), 'object of type Point cannot be compared to none-Point object!'
        if other.x > self.x and other.y > self.y:
            return True
        else:
            return False

    def __gt__(self, other):
        assert isinstance(other, Point), 'object of type Point cannot be compared to none-Point object!'
        if other.x < self.x and other.y < self.y:
            return True
        else:
            return False

    def __le__(self, other):
        assert isinstance(other, Point), 'object of type Point cannot be compared to none-Point object!'
        if other.x > self.x and other.y < self.y:
            return True
        else:
            return False

    def __ge__(self, other):
        assert isinstance(other, Point), 'object of type Point cannot be compared to none-Point object!'
        if other.x < self.x and other.y > self.y:
            return True
        else:
            return False


class Rectangle:
    """
    Takes a pair of short and a pair of long lines that should be at about 90° to each other.
    Calculates the 4 cornerpoints by finding the intersection points of those line pairs.
    Uses corner points to create new line pairs, representing the actual rectangle.
    """
    def __init__(self, short_lines, long_lines):
        assert all(isinstance(line, Line) for line in short_lines) and \
               all(isinstance(line, Line) for line in long_lines), \
               'short_lines and long_lines must be a list of lines of type Line!'
        self.short1 = short_lines[0]
        self.short2 = short_lines[1]
        self.long1 = long_lines[0]
        self.long2 = long_lines[1]
        self.cornerPoints = self.calc_corner_points()
        self.shortRes1 = Line(self.cornerPoints[3], self.cornerPoints[0])
        self.shortRes2 = Line(self.cornerPoints[1], self.cornerPoints[2])
        self.longRes1 = Line(self.cornerPoints[0], self.cornerPoints[1])
        self.longRes2 = Line(self.cornerPoints[2], self.cornerPoints[3])
        self.centerPoint = self.calc_center_point()
        self.grippingPoints = self.find_gripping_points()

    def calc_corner_points(self):
        """
        Creating the corner points of the rectangle by calculating the intersection points between the lines.
        :return: list of corner points
        """
        return [self.short1.intersection(self.long1), self.long1.intersection(self.short2),
                self.short2.intersection(self.long2), self.long2.intersection(self.short1)]

    def calc_center_point(self):
        """
        Does not calculate the real center point or point of mass of the rectangle (would be the center
        point of a perfect rectangle) but takes the diagonals and returns their intersection point.
        :return: Point object
        """
        diag1 = Line(self.cornerPoints[0], self.cornerPoints[2])
        diag2 = Line(self.cornerPoints[1], self.cornerPoints[3])
        return diag1.intersection(diag2)

    def find_gripping_points(self):
        """
        Takes previously calculated center point, and calculates the perpendicular
        foot points to both of the long lines as gripping points.
        :return: list of Points [point1, point2], gripping points
        """
        point1 = self.longRes1.perp_foot_point(self.centerPoint)
        point2 = self.longRes2.perp_foot_point(self.centerPoint)
        return [point1, point2]

    def get_gripping_points(self):
        """
        :return: self.grippingPoints as list of Points
        """
        return self.grippingPoints

    def get_res_lines(self):
        """
        :return: list of Line objects representing the rectangle
        """
        return [self.shortRes1, self.longRes1, self.shortRes2, self.longRes2]

    def __repr__(self):
        return "{}".format(self.cornerPoints)


class Line:
    """
    A Line object is defined by 2 Point objects.
    Calculates angle and length and also defines its vector and normal vector.
    Provides a variety of utility methods such as distance_point(), intersection(), etc.
    """
    def __init__(self, point1, point2):
        assert isinstance(point1, Point) and isinstance(point2, Point), \
            'arguments point1 and point2 must be of type Point!'
        self.point1 = point1
        self.point2 = point2
        self.angle = ut.line_angle(point1, point2)
        self.length = ut.line_length(point1, point2)
        # define vector and normal vector as numpy array
        self.vector = np.int16([point2.x - point1.x, point2.y - point1.y])
        self.normal = np.int16([- self.vector[1], self.vector[0]])

    def get_line(self):
        """
        :return: returns points that define the line as list [x1, y1, x2, y2]
        """
        return self.point1.point() + self.point2.point()

    def distance_point(self, point):
        """
        Calculates and returns distance of passed Point to
        the Line (not distance to line segment).
        :param point: Point object
        :return: distance value between point and line
        """
        assert isinstance(point, Point), 'argument point must be of type Point!'
        # calculate distance and return
        num = abs(self.vector[1] * point.x - self.vector[0] * point.y + self.point2.x * self.point1.y -
                  self.point2.y * self.point1.x)
        den = math.sqrt(self.vector[1]**2 + self.vector[0]**2)
        return num / den

    def distance_line_points(self, line):
        """
        Calculates and returns the average distance of the two points of the passed Line to self.
        :param line: Line object
        :return: avg distance of two line points to self
        """
        assert isinstance(line, Line), 'argument line must be of type Line!'
        return (self.distance_point(line.point1) + self.distance_point(line.point2)) / 2

    def intersection(self, line):
        """
        Calculates the intersection point between self and the passed Line.
        :param line: Line object
        :return: Point object, intersection point
        """
        assert isinstance(line, Line), 'line must be of type Line!'
        a1 = self.point1.point()
        a2 = self.point2.point()
        b1 = line.point1.point()
        b2 = line.point2.point()
        s = np.vstack([a1, a2, b1, b2])
        h = np.hstack((s, np.ones((4, 1))))
        l1 = np.cross(h[0], h[1])
        l2 = np.cross(h[2], h[3])
        x, y, z = np.cross(l1, l2)
        if z == 0:
            return None
        return Point(int(x/z), int(y/z))

    def perp_foot_point(self, point):
        """
        Calculates and returns perpendicular footpoint from passed point to self.
        :param point: Point object
        :return: Point object, perpendicular footpoint
        """
        assert isinstance(point, Point), 'point must be of type Point!'
        point2 = Point(int(point.x + self.normal[0]), int(point.y + self.normal[1]))
        perp_line = Line(point, point2)
        return self.intersection(perp_line)

    def __repr__(self):
        return "[line: {}  angle: {}  length: {}  vector: {}]".\
            format(self.get_line(), self.angle, self.length, self.vector)


class LineCluster:
    """
    Class to bundle Line objects. Takes a list of Line objects,
    also calculates their avg. angle and length values
    """
    def __init__(self, lines):
        assert all(isinstance(line, Line) for line in lines), 'lines must be a list of lines of type Line!'
        self.lines = lines
        self.avgAngle = np.sum([line.angle for line in lines])/len(lines)
        self.avgLength = np.sum([line.length for line in lines])/len(lines)

    def __repr__(self):
        return "[lines: {}; avg angle: {}; avg length: {}]".\
            format(len(self.lines), self.avgAngle, self.avgLength)


class BoundingBox:
    """
    Represents a bounding box. Takes two Point objects and takes first one as upper left
    corner point and second one as lower right corner point.
    """
    def __init__(self, point1, point2):
        assert isinstance(point1, Point) and isinstance(point2, Point), \
            'arguments point1 and point2 must be of type Point!'
        assert point1 is not point2, 'two different Points required to create BoundingBox!'
        assert point2 > point1, 'point2 must have higher x and y value than point1!'
        self.cornerPoints = [point1, point2]

    def point(self, idx):
        """
        :param idx: index to specify which point to return
        :return: Point object, corner point chosen by idx
        """
        return self.cornerPoints[idx].point()

    def points(self):
        """
        :return: List of coordinates of both points, [x1, y1, x2, y2]
        """
        points = self.point(0)
        points.extend(self.point(1))
        return points

    def __repr__(self):
        return "cornerPoints: {} {}".format(self.point(0), self.point(1))
