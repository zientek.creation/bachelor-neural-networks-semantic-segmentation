"""
bachelor - neural networks - semantic segmentation
Mask R-CNN implementation (Matterport, Inc.)

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""

import numpy as np
import cv2
import math
from . import segmentation as seg


def create_shifted_bbox(bbox, offset, source_image_shape):
    """
    Resize BoundingBox object. Does not change it's original position in image.
    Makes sure the resized BoundingBox does not stick out over the edge of the image
    :param bbox: BoundingBox object to resize
    :param offset: Offset value in pixel, how far each corner point
                    of the bbox is moved along x and y axis
    :param source_image_shape: shape of the image the bbox was in, to
                                make sure the shifted bbox does not
                                stick out over the edge of the image
    :return: shifted_bbox: BoundingBox object with shifted corner points
             list of applied offset values for corner points [x1offs, y1offs, x2offs, y2offs]
    """
    assert isinstance(bbox, seg.BoundingBox)
    # if offset of 0 is passed, return input box and zero offset list
    if not offset:
        return bbox, [0, 0, 0, 0]
    # otherwise calculate shifted bbox
    else:
        x1, y1, x2, y2 = bbox.points()
        # move upper left corner point up and left, but make
        # sure, x and y values are not smaller than 0
        if x1 >= offset:
            x1_shifted = x1 - offset
            x1_delta = -offset
        else:
            x1_shifted = 0
            x1_delta = -x1
        if y1 >= offset:
            y1_shifted = y1 - offset
            y1_delta = -offset
        else:
            y1_shifted = 0
            y1_delta = -y1
        # move lower right corner point down and right, but make
        # sure, x and y values are not bigger than image shape
        if x2 + offset < source_image_shape[0]:
            x2_shifted = x2 + offset
            x2_delta = offset
        else:
            x2_delta = source_image_shape[0] - x2
            x2_shifted = x2 + x2_delta
        if y2 + offset < source_image_shape[1]:
            y2_shifted = y2 + offset
            y2_delta = offset
        else:
            y2_delta = source_image_shape[1] - y2
            y2_shifted = y2 + y2_delta
        # create shifted bbox of type BoundingBox to return
        shifted_bbox = seg.BoundingBox(seg.Point(x1_shifted, y1_shifted), seg.Point(x2_shifted, y2_shifted))
        return shifted_bbox, [x1_delta, y1_delta, x2_delta, y2_delta]


def dist_points(point1, point2):
    """
    Calculates distance between to points of type Point
    :param point1: Point object
    :param point2: Point object
    :return: distance
    """
    assert isinstance(point1, seg.Point) and isinstance(point2, seg.Point), \
        'arguments point1 and point2 must be of type Point!'
    return math.sqrt(point1.x_dist(point2)**2 + point1.y_dist(point2)**2)


def dist_point_to_line(line, point):
    """
    Calculates the shortest distance between a point and a line (defined by two points).
    takes two line points and the point as a triangle. used method is equivalent to
    h=2A/b in a triangle
    :param line: Line object
    :param point: Point object
    :return: shortest distance
    """
    assert isinstance(line, seg.Line), 'line must be of type seg.Line!'
    assert isinstance(point, seg.Point), 'point must be of type seg.Point!'
    # numerator: twice the area of triangle of the three points
    num = abs(line.vector[1] * point.x - line.vector[0] * point.y + line.point2.x * line.point1.y -
              line.point2.y * line.point1.x)
    # denominator: distance between line points
    den = math.sqrt(line.vector[1]**2 + line.vector[0]**2)
    return num / den    # return distance value


def hough_lines_angle_length(image, config):
    """
    Applies Hough Transform to input image and returns the detected lines as list of Line objects.
    Parameters for Hough Transform are set in passed config.
    :param image: image for Hough Transform (thought to be instances.imageCanny of Image
                    object with applied Canny filter)
    :param config: ImageConfig, contains parameters for Hough Transform
    :return: list of Line objects (Lines detected by Hough Transform)
    """
    assert isinstance(config, seg.ImageConfig), 'image_config must be of type seg.ImageConfig!'
    # detect lines in input image (defined as [x1, y1, x2, y2])
    lines = cv2.HoughLinesP(image, config.rhoHough, config.thetaHough, config.thresholdHough,
                            minLineLength=config.minLineLengthHough, maxLineGap=config.maxLineGapHough)
    hough_lines = []
    # return None if Hough Transformation did not find any lines
    if lines is None:
        return None
    # create list of Line objects with all the detected lines
    for line in lines:
        x1, y1, x2, y2 = line[0]
        hough_lines.append(seg.Line(seg.Point(int(x1), int(y1)), seg.Point(int(x2), int(y2))))
    return hough_lines


def line_angle(point1, point2):
    """
    Calculates and returns angle of a line (two points) relative to the x-axis
    :param point1: Point object
    :param point2: Point object
    :return: angle in degrees
    """
    assert isinstance(point1, seg.Point) and isinstance(point2, seg.Point), \
        'arguments point1 and point2 must be of type Point!'
    # calculate x and y distance and calculate angle of pseudo complex number
    x_vec = point2.x - point1.x
    y_vec = point2.y - point1.y
    return np.angle(np.complex(x_vec, y_vec), deg=True)


def line_length(point1, point2):
    """
    Calculate length of a line of two points
    :param point1: Point object
    :param point2: Point object
    :return: length of line between two points
    """
    assert isinstance(point1, seg.Point) and isinstance(point2, seg.Point), \
        'arguments point1 and point2 must be of type Point!'
    # vectorize line and return magnitude of the vector
    x_vec = point2.x - point1.x
    y_vec = point2.y - point1.y
    return int(math.sqrt(x_vec**2 + y_vec**2))


def make_line_cluster(lines, config):
    """
    Create list of LineCluster objects, clustered by similar angle values
    :param lines: Line objects to be clustered
    :param config: ImageConfig, contains tolerances for clustering
    :return: cluster_list, list of LineCluster objects with similar angle
    """
    # return None if no lines are passed
    if lines is None:
        return None
    assert isinstance(config, seg.ImageConfig), 'image_config must be of type seg.ImageConfig!'
    assert all(isinstance(line, seg.Line) for line in lines), 'lines must be a list of lines of type seg.Line!'
    # sort lines in list of lines by angle value
    lines.sort(key=lambda line: line.angle)
    cluster_list = []    # return variable, list of LineCluster objects
    # first line (smallest angle value) to be first list element of first cluster
    lines_in_cluster = [lines[0]]
    # walk through all sorted Line objects and compare if angle
    # value is similar (in tolerance) to last Line object
    for _, line in enumerate(lines[1:]):
        # if similar angle, put line in same cluster
        if line.angle - lines_in_cluster[-1].angle < config.angleToleranceResult:
            lines_in_cluster.append(line)
        # if not similar, create LineCluster object of current line selction (cluster),
        # put it in the list of LineClusters and put the Line in new line selection for next cluster
        else:
            cluster_list.append(seg.LineCluster(lines_in_cluster))
            lines_in_cluster = [line]
    # create last LineCluster object from current line selection and add to return variable
    cluster_list.append(seg.LineCluster(lines_in_cluster))
    return cluster_list


def find_res_rectangle(cluster_list, config):
    """
    Main function of the segmentation process in this project.
    Takes a list of Clusters (ususally from single Instance in Image.instances) and
    tries to find 4 lines, that make a rectangular shape of given size with tolerance
    in size and angles.
    :param cluster_list: list of LineCluster objects
    :param config: ImageConfig object
    :return: returns Rectangle object if found, otherwise returns None
    """
    rectangles = []
    # return None if no clusters were passed into function
    if cluster_list is None:
        return None
    assert all(isinstance(cluster, seg.LineCluster) for cluster in cluster_list), \
        'cluster_list must be a list of line clusters of type seg.LineCluster!'
    assert isinstance(config, seg.ImageConfig), 'config must be of type seg.ImageConfig!'
    # pair all the clusters, that are about 90° relative to each other (with tolerances)
    # in list of RightAngleCluster objects
    right_angle_clusters = find_right_angle_cluster_pairs(cluster_list, config)
    # return None if no pairings were found
    if right_angle_clusters is None:
        return None
    # find 4 lines in RightAngleCluster forming a rectangle
    # TODO: replace next part with a function, that finds potential rectangles and takes best one regarding to area
    for pair in right_angle_clusters:
        status, rectangle_lines = check_distances_in_raclusters(pair, config)
        # if 4 matching lines found in RightAngleCluster create seg.Rectangle object
        if status:
            rectangles.append(seg.Rectangle(rectangle_lines[0], rectangle_lines[1]))
    # if no rectangles found in all the clusters return None
    if not len(rectangles):
        return None
    # if several rectangles found, take the best match/first one
    elif len(rectangles) > 1:
        print("several rectangles found, taking first one!")
    return rectangles[0]


def check_distances_in_raclusters(racluster, config):
    """
    Takes RectangleCluster and tries to find the best match of short and long sides
    to create a rectangle within the provided tolerances of their length and distance.
    :param racluster: RectangleCluster object
    :param config: seg.ImageConfig object
    :return: status (lines for rectangle found); corresponding lines, if found
    """
    assert isinstance(racluster, RightAngleCluster), 'racluster must be of type RightAngleCluster!'
    assert isinstance(config, seg.ImageConfig), 'config must be of type seg.ImageConfig!'
    # define some values provided by the config (side lengths, distances,...)
    short = config.shortSideRectangle                               # short side target length
    short_tol = config.shortSideToleranceRectangle                  # tolerance of distance between the long sides
    short_tol_factor = config.shortSideToleranceFactorRectangle     # tol. of short side length: factor * short_tol
    long = config.longSideRectangle                                 # long side target length
    long_tol = config.longSideToleranceRectangle                    # tolerance of distance between the short sides
    long_tol_factor = config.longSideToleranceFactorRectangle       # tol. of long side length: factor * long_tol
    short_dist = False              # status, if set of long sides with short distance have been found
    short_line_pairs = []           # list of line pairs with short side length and long distance
    long_dist = False               # status, if set of short sides with long distance have been found
    long_line_pairs = []            # list of line pairs with long side length and short distance

    # return None, if RightAngleCluster does not contain at least 2 lines of each angle value
    if not len(racluster.cluster1.lines) > 1 and len(racluster.cluster2.lines) > 1:
        return False, None
    # Create list of all Line pairs in first LineCluster in RightAngleCluster with short side length & long distance
    # !the following for loop is repeated four times for slightly different cases, so it's only commented the first time
    for idx, lines in enumerate(racluster.cluster1.lines):
        if long - long_tol_factor * long_tol < lines.length:    # take line only, if at least target length - tol
            # check for current line with all the following lines in current LineCluster
            for idx2, line in enumerate(racluster.cluster1.lines[idx + 1:]):
                if long - long_tol_factor * long_tol < line.length:     # go on if second line is longer than low limit
                    # check distance of current lines and decide to keep it as pair or not
                    if short - short_tol < dist_point_to_line(lines, line.point1) < short + short_tol:
                        short_dist = True   # indicates: at least one pair of long sides (short distance) was found
                        long_line_pairs.append([lines, line])   # save long line pair
    # if None found (short_dist == False), check second LineCluster in RightAngleCluster
    if not short_dist:
        for idx, lines in enumerate(racluster.cluster2.lines):
            if long - long_tol_factor * long_tol < lines.length:
                for idx2, line in enumerate(racluster.cluster2.lines[idx + 1:]):
                    if long - long_tol_factor * long_tol < line.length:
                        if short - short_tol < dist_point_to_line(lines, line.point1) < short + short_tol:
                            short_dist = True
                            long_line_pairs.append([lines, line])
        # if no match found in both LineClusters (short sides with long distance) return None
        if not short_dist:
            return False, None
        # otherwise check other LineCluster for long line pairs with short distance
        else:
            for idx, lines in enumerate(racluster.cluster1.lines):
                if short - short_tol_factor * short_tol < lines.length:
                    for line in racluster.cluster1.lines[idx + 1:]:
                        if short - short_tol_factor * short_tol < line.length:
                            if long - long_tol < dist_point_to_line(lines, line.point1) < long + long_tol:
                                long_dist = True
                                short_line_pairs.append([lines, line])
    # if short side pair with long distance found in 1st LineCluster, check 2nd cluster for short sides w. long distance
    else:
        for idx, lines in enumerate(racluster.cluster2.lines):
            if short - short_tol_factor * short_tol < lines.length:
                for line in racluster.cluster2.lines[idx + 1:]:
                    if short - short_tol_factor * short_tol < line.length:
                        if long - long_tol < dist_point_to_line(lines, line.point1) < long + long_tol:
                            long_dist = True
                            short_line_pairs.append([lines, line])
    # if long side pairs (short distance) found but no short side pairs (long distance) return None
    if not long_dist:
        return False, None
    # extract best match of short line pairs closest to target distance (long)
    if len(short_line_pairs) > 1:
        print('several short lines with long side distance found, taking best match!')
    short_lines = find_best_result(short_line_pairs, long)
    # extract best match of long line pairs closest to target distance (short)
    if len(long_line_pairs) > 1:
        print('several long lines with short side distance found, taking best match!')
    long_lines = find_best_result(long_line_pairs, short)
    return True, [short_lines, long_lines]


def find_best_result(line_pairs, distance):
    """
    Takes list of pairs of seg.Line objects, and a distance value. Returns the one line
    pair with the distance between the lines closest to provided distance value.
    :param line_pairs: List of pairs of seg.Line objects -> [[Line, Line], [Line, Line],...]
    :param distance:
    :return:
    """
    # Take first Line pair in line_pairs as best result and best match for distance value
    best_result = line_pairs[0]
    best_distance = abs(distance - line_pairs[0][0].distance_line_points(line_pairs[0][1]))
    # if more than one pair of Lines in line_pairs, check all pairs for better distance match
    if len(line_pairs) > 1:
        for idx, pair in enumerate(line_pairs[1:]):
            dist = abs(distance - pair[0].distance_line_points(pair[1]))    # get avg. distance between Lines
            # if better match of distance value in current Line pair, take as best result
            if dist < best_distance:
                best_distance = dist
                best_result = pair
    return best_result


class RightAngleCluster:
    """
    Contains two objects of type seg.LineCluster, expected to be at 90° relative to each other
    """
    def __init__(self, cluster1, cluster2):
        assert isinstance(cluster1, seg.LineCluster) and isinstance(cluster2, seg.LineCluster), \
            "Both cluster arguments must be of type seg.LineCluster!"
        self.cluster1 = cluster1
        self.cluster2 = cluster2


def find_right_angle_cluster_pairs(cluster_list, config):
    """
    Takes a List of seg.LineCluster objects and a config. Finds LineClusters in List
    that are at 90° relative to each other and creates RightAngleCluster obj. Returns
    list of resultant RightAngleClusters
    :param cluster_list: List of seg.LineCluster objects
    :param config: object of type seg.ImageConfig
    :return: list of RightAngleCluster objects
    """
    right_angle_clusters = []
    # if any clusters passed, check which clusters are at 90° (with tolerance)
    # to each other and create list of RightAngleCluster objects
    if len(cluster_list):
        for idx, cluster in enumerate(cluster_list):
            for idx2, clust in enumerate(cluster_list[idx + 1:]):
                # create RightAngleCluster if relative angle between both clusters is within 90° +- tolerance
                if (90 - config.angleToleranceResult) < abs(cluster.avgAngle - clust.avgAngle) \
                        < (90 + config.angleToleranceResult):
                    right_angle_clusters.append(RightAngleCluster(cluster, clust))
    # if no cluster pairs found with 90° return None
    if not len(right_angle_clusters):
        return None
    return right_angle_clusters


def pred_result_to_bboxes(rois):
    """
    Takes list of ROIs (region of interest) typically produced by neural network on prediction on image.
    Converts it to list of seg.BoundingBox objects to be used in seg.Image.add_instances().
    Uses roi_to_bbox() for conversion.
    :param rois: list of ROIs -> [[x1, y1, x2, y2], [x1, y1, x2, y2],...] (upper left and bottom right corner point)
    :return: list of seg.BoundingBox objects
    """
    bboxes = []
    # get seg.BoundingBox object for every roi in passed list
    for roi in rois:
        bboxes.append(roi_to_bbox(roi))
    return bboxes


def roi_to_bbox(roi):
    """
    Converts single roi to seg.BoundingBox object
    :param roi: ROI -> [x1, y1, x2, y2]  (upper left and bottom right corner points from roi)
    :return: seg.BoundingBox object, created by roi
    """
    x1, y1, x2, y2 = roi
    return seg.BoundingBox(seg.Point(int(x1), int(y1)), seg.Point(int(x2), int(y2)))


def line_intersection(line1, line2):
    """
    Takes two lines of type seg.Line and returns their intersection point.
    Returns None, if lines are parallel.
    :param line1: seg.Line object
    :param line2: seg.Line object
    :return: seg.Point obj., Point where line1 and line2 intersect
    """
    assert isinstance(line1, seg.Line) and isinstance(line2, seg.Line), 'line1 and line2 must be of type seg.Line!'
    a1 = line1.point1.point()
    a2 = line1.point2.point()
    b1 = line2.point1.point()
    b2 = line2.point2.point()
    # build Matrix and extend to 3D (add z-value = 1 for each point)
    s = np.vstack([a1, a2, b1, b2])
    h = np.hstack((s, np.ones((4, 1))))
    # getting line vectors with cross product
    l1 = np.cross(h[0], h[1])
    l2 = np.cross(h[2], h[3])
    # calculating intersection point of lines using cross product
    x, y, z = np.cross(l1, l2)
    if z == 0:
        return None
    # divide x and y value by z to get 2D intersection Point
    return seg.Point(int(x/z), int(y/z))


def perp_foot_point(line, point):
    """
    Takes seg.Line object and seg.Point object and calculates perpendicular foot point on the line.
    Creates line, that is perpendicular to passed line and contains passed Point.
    Calculates and returns intersection point of both lines.
    :param line: seg.Line object
    :param point: sef.Point object
    :return: seg.Point object, perpendicular foot point
    """
    assert isinstance(line, seg.Line), 'line must be of type seg.Line!'
    assert isinstance(point, seg.Point), 'point must be of type seg.Point!'
    # create point to create perpendicular line by moving first point by norm vector of line.
    point2 = seg.Point(int(point.x + line.normal[0]), int(point.y + line.normal[1]))
    # create perpendicular line
    perp_line = seg.Line(point, point2)
    # return intersection point of both lines
    return line.intersection(perp_line)


def draw_results_res_image(target_image, result, config, offset=None):
    """
    Draws passed results on passed image (rectangle, center point and gripping points)
    Can either be used to draw on instance.imageResult or on Image.resImage (just pass first point coordinates
    of instance.shiftedBbox as offset)
    :param target_image: image to draw the results on, thought to be Image.resImage (does not copy the image)
    :param result: seg.InstanceResult object numpyeg.Image.instances[.].instanceResult)
    :param config: seg.ImageConfig object
    :param offset: offset of where to count pixels of results in image (thought to first Point of Instance.shiftedBbox)
    :return: nothing
    """
    assert isinstance(result, seg.InstanceResult), 'result must be of type seg.InstanceResult!'
    assert isinstance(config, seg.ImageConfig), 'config must be of type seg.ImageConfig!'

    # nested functions to draw different parts of seg.Instance.instanceResult
    def draw_rectangle(image, res, conf):
        """
        Takes rectangle from result and draws it onto the passed image.
        Settings (color, line,...) can be modified in imageConfig.
        :param image: image to draw rectangle on (numpy)
        :param res: seg.InstanceResult object, corresponding to passed image
        :param conf: seg.ImageConfig obj, needed for some parameters (color,...)
        :return: nothing
        """
        # draw all lines of the rectangle in passed result on
        # passed image, if not None
        if result.rectangle is not None:
            lines = res.rectangle.get_res_lines()
            for line in lines:
                cv2.line(image, tuple(line.point1.point()), tuple(line.point2.point()),
                         conf.lineColorImageResult, conf.lineThicknessImageResult)

    def draw_center_point(image, res, conf):
        """
        Takes center point from result.rectangle and draws it onto passed image.
        Settings can be made in ImageConfig.
        :param image: image to draw center point on (seg.Image.instanceResult.imageResult)
        :param res: seg.InstanceResult object, corresponding to passed image
        :param conf: seg.ImageConfig obj, needed for some parameters (color,...)
        :return: nothing
        """
        # draw center point of rectangle of passed result on
        # passed image, if not None
        if res.rectangle is not None:
            point = res.rectangle.centerPoint
            cv2.circle(image, tuple(point.point()), conf.centerPointRadiusImageResult,
                       conf.centerPointColorImageResult, thickness=-1)

    def draw_gripping_points(image, res, conf):
        """
        Takes gripping points from result and draws them onto passed image.
        Settings can be made in ImageConfig.
        :param image: image to draw gripping points on (seg.Image.instanceResult.imageResult)
        :param res: seg.InstanceResult object, corresponding to passed image
        :param conf: seg.ImageConfig obj, needed for some parameters (color,...)
        :return: nothing
        """
        # draws gripping points of passed result on
        # passed image, if not None
        points = res.grippingPoints
        if points is not None:
            for point in points:
                cv2.circle(image, tuple(point.point()), conf.grippingPointRadiusImageResult,
                           conf.grippingPointColorImageResult, thickness=-1)
    # set default offset
    if offset is None:
        offset = [0, 0]
    # call nested functions to draw all results
    # offset does, that only part of the image gets passed into function, so
    # results can be drawn on original image, where instance was found
    draw_rectangle(target_image[offset[0]:, offset[1]:], result, config)
    draw_center_point(target_image[offset[0]:, offset[1]:], result, config)
    draw_gripping_points(target_image[offset[0]:, offset[1]:], result, config)


def resize_image(image, config):
    """
    Resizes image, keeping its original ratio, so that none of the sides is longer
    than configured in passed seg.ImageConfig (heightImage & widthImage) but at
    least one side exactly matches.
    :param image: image to resize (numpy)
    :param config: seg.ImageConfig
    :return: scaled image (numpy)
    """
    assert isinstance(config, seg.ImageConfig), 'config must be of type seg.ImageConfig!'
    # get shape of original image and limits in configuration
    orig_h, orig_w = image.shape[:2]
    ratio = orig_w / orig_h
    height = config.heightImage
    width = config.widthImage
    # choose right interpolation method by finding out if
    # image is gonna be smaller or bigger after resizing
    if orig_h > height or orig_w > width:
        interp = cv2.INTER_AREA
    else:
        interp = cv2.INTER_CUBIC

    # define new width and height
    # make sure no side is bigger than
    # the set limit in passed config
    w = width
    h = round(w / ratio)
    if h > height:
        h = height
        w = round(h * ratio)
    # scale image and return it
    scaled_image = cv2.resize(image, (w, h), interpolation=interp)
    return scaled_image


def yolo_results_to_image_bboxes(results):
    """
    Converts Yolo result output to List of seg.BoundingBox objects
    :param results: YOLOv2 prediction results
    :return: list of seg.BoundingBox objects
    """
    # create list of ROIs with all results in passed list
    bbox_list = []
    for result in results:
        x1 = result['topleft']['y']
        y1 = result['topleft']['x']
        x2 = result['bottomright']['y']
        y2 = result['bottomright']['x']
        bbox_list.append([x1, y1, x2, y2])
    # convert list of ROIs to List of
    # seg.BoundingBox objects and return
    return pred_result_to_bboxes(bbox_list)
