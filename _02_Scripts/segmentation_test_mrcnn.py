"""
bachelor - neural networks - semantic segmentation
Segmentation Mask R-CNN Test

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""

from _02_Scripts import segmentation as seg
from _02_Scripts import mask_rcnn as nn
import os
import cv2

# create default NeuralNetConfig for Mask R-CNN
neural_net_config = nn.NeuralNetConfig()
neural_net = nn.NeuralNet(neural_net_config)

PATH = os.getcwd()
print(PATH)
# run test code on all images in provided folder
for _, _, files in os.walk(os.path.join(PATH, '../_01_Dataset/test/test/')):
    for filename in files:
        if filename.endswith(".jpg") or filename.endswith(".png"):
            img_PATH = os.path.join(PATH, '../_01_Dataset/test/test/' + filename)
            img = cv2.imread(img_PATH)          # load current image
            imageConfig = seg.ImageConfig()     # create Config and Image obj
            image = seg.Image(img, imageConfig)
            # run prediction on image in Image obj
            # creates instances for detected objects and runs
            # segmentation process in every instance in Image object
            neural_net.predict(image)
            # show results
            image.visualize_instances()
            # changes in config to show the effect
            image.config.bboxOffsetInstance = 10
            image.config.rectColorResImage = (255, 255, 50)
            image.recreate_all_instances()
            # show again with modifications
            image.visualize_instances()
