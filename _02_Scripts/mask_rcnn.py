"""
bachelor - neural networks - semantic segmentation
Mask R-CNN implementation (Matterport, Inc.)

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""


import os
import numpy as np
import json
import time
from PIL import Image, ImageDraw
from _02_Scripts import segmentation as seg
from _02_Scripts import segmentation_utils as ut
from _03_Mask_RCNN.mrcnn.config import Config
import _03_Mask_RCNN.mrcnn.utils as utils
import _03_Mask_RCNN.mrcnn.model as modellib


class NeuralNet:
    """
    Implementation of Matterports Mask R-CNN, used for this projects
    purpose of detecting objects, pass the results into seg.Image object
    and trigger semantic segmentation process.
    Has to be configured with NeuralNetConfig
    """
    def __init__(self, config):
        assert isinstance(config, NeuralNetConfig), 'argument config must be of type NeuralNetConfig!'
        root_mask_rcnn = config.rootPath
        assert os.path.exists(root_mask_rcnn), 'path root_mask_rcnn does not exist!'
        self.config = config
        model_dir = os.path.join(root_mask_rcnn, self.config.modelDir)
        self.config.config.display()
        # create actual Mask R-CNN model
        self.model = modellib.MaskRCNN(mode=self.config.mode,
                                       config=self.config.config,
                                       model_dir=model_dir)
        # declare model Path, after load_weights() contains path to loaded model weights
        self.modelPath = ''
        # load weights as configured in NeuralNetConfig
        self.load_weights()

    def predict(self, image):
        """
        Executes prediction on input image.
        Input:
            image: object of type seg.Image
        Does not return anything since the results of the
        prediction are passed to the image obj as new instances.
        """
        assert isinstance(image, seg.Image), 'argument image must be of type seg.Image!'
        # find objects in image of seg.Image obj and get them converted to list of seg.BoundingBoxes
        pred_results = ut.pred_result_to_bboxes(self.model.detect([image.image], verbose=1)[0]['rois'])
        # passes bboxes to Image obj. and triggers segmentation process
        image.add_instances(pred_results)

    def train(self, train_dataset, val_dataset, learning_rate, epochs, layers,
              augmentation=None, custom_callbacks=None, no_augmentation_sources=None):
        """
        Train Mask R-CNN model on provided training data.
        Input:
            train_dataset: training utils.Dataset object
            val_dataset: validation utils.Dataset object
            learning_rate: Learning rate to train with
            epochs: number of training epochs
                    !does not add epochs to previous epochs
                    !e.g. you load 25 epochs trained weights and provide epochs=40
                    !15 additional epochs will be trained, not 40
            layers: allows selection of layers to train (usually 'heads' or 'all')
                    - One of these predefined values:
                        heads: The RPN, classifier and mask heads of the network
                        all: All the layers
                        3+: Train Resnet stage 3 and up
                        4+: Train Resnet stage 4 and up
                        5+: Train Resnet stage 5 and up
            augmentation: optional. takes simple or complex data generators such as imgaug.augmenters.
            custom_callbacks: optional. custom callbacks to be calles with the keras fit generator method.
                                must be list of type keras.callbacks
            no_augmentation_sources: optional. list of sources to exclude for augmentation.
                                        String that identifies a dataset and is defines in the Dataset class.
        """
        # get time at start of training to provide total training time at the end
        start_train = time.time()
        # train model with provided settings (config and parameters)
        self.model.train(train_dataset, val_dataset, learning_rate, epochs, layers,
                         augmentation=augmentation, custom_callbacks=custom_callbacks,
                         no_augmentation_sources=no_augmentation_sources)
        # calculate and print training time at the end of training
        end_train = time.time()
        minutes = round((end_train - start_train) / 60, 2)
        print(f'Training took {minutes} minutes.')

    def load_weights(self):
        """
        Load model weights depending of configuration
            last: find most trained weights of given model name (NeuralNetConfig.config.NAME)
                    in modeldir provided in model
            coco: load coco weights from mrcnn root directory
        """
        # for 'last' try to find last trained weights in modeldir and load
        if self.config.weights == 'last':
            self.modelPath = self.model.find_last()
            assert self.modelPath != "", "Path to trained weights (self.model.find_last()) does not exist!"
            self.model.load_weights(self.model.find_last(), by_name=True)
            print('load weights from ', self.modelPath)
        # for 'coco' load original coco weights provided in mrcnn root path
        elif self.config.weights == 'coco':
            self.modelPath = os.path.join(self.config.rootPath, "mask_rcnn_coco.h5")
            self.model.load_weights(self.modelPath, by_name=True,
                                    exclude=["mrcnn_class_logits", "mrcnn_bbox_fc", "mrcnn_bbox", "mrcnn_mask"])
            print('load coco weights from ', os.path.join(self.config.rootPath, "mask_rcnn_coco.h5"))

    def __repr__(self):
        return "architecture: {}; weights loaded from: {}".format(self.config.config.BACKBONE,
                                                                  self.modelPath)


class NeuralNetConfig:
    """
    Configuration class for Mask R-CNN.
    Input:
        mode: optional. configures model mode. Takes 'inference'(default) or 'training'.
        weights: optional. affects what weights are getting loaded into the model.
                    last: (default) find last trained weights of model with same name (NeuralNetConfig.config.NAME)
                    coco: load original coco weights, provided in mrcnn root directory
        rootpath: optional. relative path from this __file__ to mrcnn root directory (default: '../_03_Mask_RCNN')
        modeldir: optional. relative path from mrcnn root directory to log directory (default: 'logs')

    Values of NeuralNetConfig.config can be changed after creating NeuralNetConfig object by simply assigning value
    to NeuralNetConfig.config.xxx
    Properties, that can be configured:
        NAME, GPU_COUNT, IMAGES_PER_GPU, STEPS_PER_EPOCH, VALIDATION_STEPS, BACKBONE, COMPUTE_BACKBONE_SHAPE,
        BACKBONE_STRIDES, FPN_CLASSIF_FC_LAYERS_SIZE, TOP_DOWN_PYRAMID_SIZE, NUM_CLASSES, RPN_ANCHOR_SCALES,
        RPN_ANCHOR_RATIOS, RPN_ANCHOR_STRIDE, RPN_NMS_THRESHOLD, RPN_TRAIN_ANCHORS_PER_IMAGE, PRE_NMS_LIMIT,
        POST_NMS_ROIS_TRAINING, POST_NMS_ROIS_INFERENCE, USE_MINI_MASK, MINI_MASK_SHAPE, IMAGE_RESIZE_MODE,
        IMAGE_MIN_DIM, IMAGE_MAX_DIM, IMAGE_MIN_SCALE, IMAGE_CHANNEL_COUNT, MEAN_PIXEL, TRAIN_ROIS_PER_IMAGE,
        ROI_POSITIVE_RATIO, POOL_SIZE, MASK_POOL_SIZE, MASK_SHAPE, MAX_GT_INSTANCES, RPN_BBOX_STD_DEV,
        BBOX_STD_DEV, DETECTION_MAX_INSTANCES, DETECTION_MIN_CONFIDENCE, DETECTION_NMS_THRESHOLD, LEARNING_RATE,
        LEARNING_MOMENTUM, WEIGHT_DECAY, LOSS_WEIGHTS, USE_RPN_ROIS, TRAIN_BN, GRADIENT_CLIP_NORM
    More detailed information about the properties to find in Project WIKI:
    https://gitlab.com/zientek.creation/bachelor-neural-networks-semantic-segmentation/-/wikis/Training/01-Mask-R-CNN
    """
    class InferenceConfig(Config):
        """
        Default configuration for inference mode of NeuralNet obj.
        """
        NAME = "res50_ho_aug_2"
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1
        IMAGE_MIN_DIM = 640
        IMAGE_MAX_DIM = 640
        DETECTION_MIN_CONFIDENCE = 0.85
        BACKBONE = "resnet50"
        NUM_CLASSES = 1 + 1

    class TrainConfig(Config):
        """
        Default configuration for training mode of NeuralNet obj.
        """
        NAME = "res50_ho_aug_2"
        GPU_COUNT = 1
        IMAGES_PER_GPU = 2
        NUM_CLASSES = 1 + 1
        IMAGE_MIN_DIM = 640
        IMAGE_MAX_DIM = 640
        STEPS_PER_EPOCH = 100
        VALIDATION_STEPS = 200
        BACKBONE = 'resnet50'
        RPN_ANCHOR_SCALES = (32, 64, 128, 25, 512)
        TRAIN_ROIS_PER_IMAGE = 200
        MAX_GT_INSTANCES = 100
        DETECTION_MIN_CONFIDENCE = 0.8
        POST_NMS_ROIS_INFERENCE = 1000
        POST_NMS_ROIS_TRAINING = 2000

    def __init__(self, mode='inference', weights='last', rootpath='../_03_Mask_RCNN/', modeldir='logs'):
        # set provided mode and load corresponding default configuration
        self.mode = mode
        if self.mode == 'inference':
            self.config = self.InferenceConfig()
        elif self.mode == 'training':
            self.config = self.TrainConfig()
        # assignment of more attributes
        self.rootPath = rootpath
        self.modelDir = modeldir
        self.weights = weights

    def __repr__(self):
        return "config for NeuralNet class"


####################################################################################
# CocoLikeDataset class
####################################################################################

# the following class CokoLikeDataset is copied from Adam Kelly's cocosynth repo:
# https://github.com/akTwelve/cocosynth/blob/master/notebooks/train_mask_rcnn.ipynb
#
# it takes coco-annotations and corresponding images and generates a coco like dataset
# which can be used to train Matterport's Mask R-CNN


class CocoLikeDataset(utils.Dataset):
    """
    Generates a COCO-like dataset, i.e. an image dataset annotated in the style of the COCO dataset.
    See http://cocodataset.org/#home for more information.
    """

    def load_data(self, annotation_json, images_dir):
        """
        Load the coco-like dataset from json
        Args:
            annotation_json: The path to the coco annotations json file
            images_dir: The directory holding the images referred to by the json file
        """
        # Load json from file
        json_file = open(annotation_json)
        coco_json = json.load(json_file)
        json_file.close()

        # Add the class names using the base method from utils.Dataset
        source_name = "coco_like"
        for category in coco_json['categories']:
            class_id = category['id']
            class_name = category['name']
            if class_id < 1:
                print('Error: Class id for "{}" cannot be less than one. (0 is reserved for the background)'.format(
                    class_name))
                return

            self.add_class(source_name, class_id, class_name)

        # Get all annotations
        annotations = {}
        for annotation in coco_json['annotations']:
            image_id = annotation['image_id']
            if image_id not in annotations:
                annotations[image_id] = []
            annotations[image_id].append(annotation)

        # Get all images and add them to the dataset
        seen_images = {}
        for image in coco_json['images']:
            image_id = image['id']
            if image_id in seen_images:
                print("Warning: Skipping duplicate image id: {}".format(image))
            else:
                seen_images[image_id] = image
                try:
                    image_file_name = image['file_name']
                    image_width = image['width']
                    image_height = image['height']
                except KeyError as key:
                    print("Warning: Skipping image (id: {}) with missing key: {}".format(image_id, key))

                image_path = os.path.abspath(os.path.join(images_dir, image_file_name))
                image_annotations = annotations[image_id]

                # Add the image using the base method from utils.Dataset
                self.add_image(
                    source=source_name,
                    image_id=image_id,
                    path=image_path,
                    width=image_width,
                    height=image_height,
                    annotations=image_annotations
                )

    def load_mask(self, image_id):
        """
        Load instance masks for the given image.
        MaskRCNN expects masks in the form of a bitmap [height, width, instances].
        Args:
            image_id: The id of the image to load masks for
        Returns:
            masks: A bool array of shape [height, width, instance count] with
                one mask per instance.
            class_ids: a 1D array of class IDs of the instance masks.
        """
        image_info = self.image_info[image_id]
        annotations = image_info['annotations']
        instance_masks = []
        class_ids = []

        for annotation in annotations:
            class_id = annotation['category_id']
            mask = Image.new('1', (image_info['width'], image_info['height']))
            mask_draw = ImageDraw.ImageDraw(mask, '1')
            for segmentation in annotation['segmentation']:
                mask_draw.polygon(segmentation, fill=1)
                bool_array = np.array(mask) > 0
                instance_masks.append(bool_array)
                class_ids.append(class_id)

        mask = np.dstack(instance_masks)
        class_ids = np.array(class_ids, dtype=np.int32)

        return mask, class_ids
