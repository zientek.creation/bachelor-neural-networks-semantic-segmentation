# Bachelor project - Neural Networks - Object Detection with Instance Segmentation and Subsequent Image Recognition in Python
The general idea of the project is, not only detecting a work object and its 
position in a camera image but also recognize its orientation, to be able to get
output how to pick it up with a robot. Since writing own layers to a neural net
can be very challenging, this solution combines the prediction of a neural net 
with image recognition with *openCV*.
The work object to recognize is a wooden building brick.
Due to the actual situation with Corona, picking up the work object with a robot
is not part of this project. The goal is to get an output of coordinates of two 
gripping points for each detected object.

1. Creating Image object for input image
2. Predict positions of work objects in the image (bounding boxes)
3. Adding predicted boxes as Instance objects to Image object
4. Run segmentation on each Instance to find coordinates of gripping points
5. Results for each Instance are contained in Image object


# [WIKI](https://gitlab.com/zientek.creation/bachelor-neural-networks-semantic-segmentation/-/wikis/home)
For more detailed information on how to use the project check out the project 
wiki. There are guides on how to be able to use the project on your local 
machine or how to run it on Google Colab.