"""
bachelor - neural networks - semantic segmentation
YOLOv2 dataset preparation

Copyright (c) 2020 Simon Zientek
Licensed under the MIT License (see LICENSE for details)
Written by Simon Zientek
"""

import os

# define important Paths and require, that they exist
ROOT_DIR = os.path.join(os.getcwd(), '..')
assert os.path.exists(ROOT_DIR), 'ROOT_DIR does not exist!'
TRAIN_DIR = os.path.join(ROOT_DIR, '_04_yolov2/darkflow-master/dataset/train/images')
assert os.path.exists(TRAIN_DIR), 'TRAIN_DIR does not exist!'
VAL_DIR = os.path.join(ROOT_DIR, '_01_Dataset/val_yolov2/images/')
assert os.path.exists(VAL_DIR), 'VAL_DIR does not exist!'

print('ROOT_DIR:', ROOT_DIR)
print('TRAIN_DIR:', TRAIN_DIR)
print('VAL_DIR:', VAL_DIR)

# create text files that contain paths to all dataset
# images for validation and training dataset
file_train = open(os.path.join(TRAIN_DIR, '../train.txt'), 'w')
file_val = open(os.path.join(VAL_DIR, '../val.txt'), 'w')

# walk through train dataset directory, find all images (.jpg/.png)
# and write paths in corresponding text file
for root, dirs, files in os.walk(TRAIN_DIR):
    for name in files:
        _, ext = os.path.splitext(os.path.basename(name))
        if ext == '.png' or ext == '.jpg':
            print(os.path.join(TRAIN_DIR, name))
            file_train.write(os.path.join(TRAIN_DIR, name) + "\n")

# walk through validation dataset directory, find all images (.jpg/.png)
# and write paths in corresponding text file
for root, dirs, files in os.walk(VAL_DIR):
    for name in files:
        _, ext = os.path.splitext(os.path.basename(name))
        if ext == '.png' or ext == '.jpg':
            print(os.path.join(VAL_DIR, name))
            file_val.write(os.path.join(VAL_DIR, name) + "\n")
